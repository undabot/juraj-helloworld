//
//  AppDelegate.h
//  HelloWorld
//
//  Created by Juraj Selanec on 07/09/15.
//  Copyright (c) 2015 Juraj Selanec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

