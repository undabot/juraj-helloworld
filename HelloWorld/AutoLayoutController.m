#import "AutoLayoutController.h"


@interface AutoLayoutController()

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *countTitleLabel;
@property (strong, nonatomic) UIButton *appendButton;
@end

@implementation AutoLayoutController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addLabels];
    [self addButton];
    [self addConstraints];
}

- (void)addLabels
{
    self.titleLabel.text = @"Hello World";
    [self updateCountLabelForString:self.titleLabel.text];
    
    [self.view addSubview:self.titleLabel];
    [self.view addSubview:self.countTitleLabel];
    
}

- (void)addConstraints
{
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@0).with.offset(15);
        make.left.right.equalTo(@0);
    }];
    
    __weak typeof(self) weakSelf = self;
    [self.countTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@0).with.offset(10);
        make.top.equalTo(weakSelf.titleLabel.mas_bottom).with.offset(10);
    }];
    
    [self.appendButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.countTitleLabel);
        make.left.equalTo(weakSelf.countTitleLabel.mas_right).with.offset(20);
        make.width.equalTo(@70);
   }];
}

- (void)addButton
{
    [self.appendButton addTarget:self action:@selector(appendButtonClick) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:self.appendButton];
}

- (void)appendButtonClick
{
    [self appendTitleLabelString];
    [self updateCountLabelForString:self.titleLabel.text];
}

- (void)appendTitleLabelString
{
    NSMutableString *newTitleLabelString = [self.titleLabel.text mutableCopy];
    [newTitleLabelString appendString:newTitleLabelString];
    self.titleLabel.text = newTitleLabelString;
}


- (void)updateCountLabelForString:(NSString *)title
{
    self.countTitleLabel.text = [NSString stringWithFormat:@"%lu", title.length];
}



#pragma mark - Properties

- (UILabel*) titleLabel
{
    if (!_titleLabel){
        _titleLabel = [UILabel new];
        _titleLabel.lineBreakMode = NSLineBreakByCharWrapping;
        _titleLabel.numberOfLines = 0;
    }
    return _titleLabel;
}

- (UILabel*) countTitleLabel
{
    if (!_countTitleLabel){
        _countTitleLabel = [UILabel new];
    }
    return _countTitleLabel;
}

- (UIButton*) appendButton
{
    if (!_appendButton){
         _appendButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _appendButton.backgroundColor = [UIColor redColor];
        [_appendButton setTitle:@"Button" forState:UIControlStateNormal];
                         
    }
    return _appendButton;
}



@end