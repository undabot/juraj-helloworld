#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ClockParity : NSObject

@property (strong, nonatomic) UIColor *currentColor;

- (BOOL)sholudChangeBackgroundColorOnDate:(NSDate *)date;
- (UIColor *)getNewColor;
@end
