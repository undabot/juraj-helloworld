#import "ClockParity.h"

@interface ClockParity()
@end

@implementation ClockParity

- (BOOL)sholudChangeBackgroundColorOnDate:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitHour  fromDate:date];
    NSInteger hour = [components hour];
    
    if ([self.currentColor isEqual:[UIColor redColor]]) {
        return (hour % 2) ? NO : YES;
        
    } else {
        return (hour % 2) ? YES : NO;
    }
}

- (UIColor *)getNewColor
{
    return ([self.currentColor isEqual:[UIColor redColor]])? [UIColor greenColor] : [UIColor redColor];
}
@end
