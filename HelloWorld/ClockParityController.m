#import "ClockParityController.h"
#import "ClockParity.h"

@interface ClockParityController()
@property (strong, nonatomic) UILabel *clockLabel;
@property (strong, nonatomic) ClockParity *clockParity;
@end

@implementation ClockParityController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addLabel];
    
    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateClockLabelText) userInfo:nil repeats:YES];
}

- (void)addLabel
{
    [self.view addSubview:self.clockLabel];
    
    [self.clockLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(@0);
    }];
}

- (void)updateClockLabelText
{
    NSDate *date= [NSDate date];
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.dateFormat = @"HH:mm:ss";    
    self.clockLabel.text = [formatter stringFromDate:date];
    
    if ([self.clockParity sholudChangeBackgroundColorOnDate:date])
    {
        [self changeBackgroundOfLabel:self.clockLabel inColor:[self.clockParity getNewColor]];
    }
}

- (void)changeBackgroundOfLabel:(UILabel *)label inColor:(UIColor *)color
{
    self.clockParity.currentColor=color;
    label.backgroundColor = color;
}

#pragma mark - Properties

- (UILabel *)clockLabel
{
    if (!_clockLabel){
        _clockLabel = [UILabel new];
        _clockLabel.backgroundColor = [UIColor greenColor];
    }
    return _clockLabel;
}

- (ClockParity *)clockParity
{
    if (!_clockParity){
        _clockParity = [[ClockParity alloc] init];
        _clockParity.currentColor = [UIColor greenColor];
    }
    return _clockParity;
}


@end
