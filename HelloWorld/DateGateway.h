#import <Foundation/Foundation.h>

@protocol DateGateway <NSObject>

- (RACSignal *)dateForInterval:(NSTimeInterval)interval;

@end