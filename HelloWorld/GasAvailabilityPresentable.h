#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface GasAvailabilityPresentable : NSObject
@property (strong, nonatomic) NSString *labelText;
@property (strong, nonatomic) UIColor *backgroundColor;
@end
