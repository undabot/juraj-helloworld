#import <Foundation/Foundation.h>
#import "DateGateway.h"

@interface GasStationUseCase : NSObject

- (RACSignal *)gasAvailabilityChangedSignal;
- (instancetype)initWithDateGateway:(id<DateGateway>)dateGateway;

@end