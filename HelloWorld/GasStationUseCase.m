#import "GasStationUseCase.h"

//presentable
#import "GasAvailabilityPresentable.h"

@interface GasStationUseCase()
@property (nonatomic, strong) id<DateGateway> dateGateway;
@end

@implementation GasStationUseCase

static NSTimeInterval const clockInterval = 1;

- (instancetype)initWithDateGateway:(id<DateGateway>)dateGateway
{
    self = [super init];
    if (self) {
        _dateGateway = dateGateway;
    }
    return self;
}

#pragma mark - Public

- (RACSignal *)gasAvailabilityChangedSignal
{
    @weakify(self);
    return [[self.dateGateway  dateForInterval:clockInterval] flattenMap:^RACStream *(NSDate *dateInterval) {
            @strongify(self);
            GasAvailabilityPresentable *status = [self gasAvailabilityPresentableForDate:dateInterval];
            return [RACSignal return:status];        
    }];
}

#pragma mark - Private

- (GasAvailabilityPresentable *)gasAvailabilityPresentableForDate:(NSDate *)date
{
    GasAvailabilityPresentable *status = [GasAvailabilityPresentable new];
    status.labelText = [self getNewLabelStringFromDate:date];
    status.backgroundColor = [self colorForDate:date];
    return status;
}

- (NSString *)getNewLabelStringFromDate:(NSDate *)date
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.dateFormat = @"HH:mm:ss";
    return [formatter stringFromDate:date];
}

- (UIColor *)colorForDate:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitHour  fromDate:date];
    NSInteger hour = [components hour];
    
    if (hour % 2 == 0) {
        return [UIColor greenColor];
    } else {
        return [UIColor redColor];
    }
}

@end