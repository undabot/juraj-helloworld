#import "GasStatusViewController.h"

//use case
#import "GasStationUseCase.h"
#import "GasAvailabilityPresentable.h"
#import "InMemoryDateGateway.h"

//libs
#import <Masonry/Masonry.h>

@interface GasStatusViewController()

@property (strong, nonatomic) UILabel *clockLabel;
@property (nonatomic, strong) GasStationUseCase *useCase;
@property (nonatomic, strong) InMemoryDateGateway *dateGateway;

@end

@implementation GasStatusViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addLabel];
    [self addUseCaseEventListeners];
}

- (void)addUseCaseEventListeners
{
    @weakify(self);
    [[self.useCase gasAvailabilityChangedSignal] subscribeNext:^(GasAvailabilityPresentable *gasPresentable) {
        @strongify(self);
        [self gasAvailabilityChanged:gasPresentable];
    }];
}

- (void)addLabel
{
    [self.view addSubview:self.clockLabel];
    [self.clockLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(@0);
    }];
}

#pragma mark - GasStatusDelegate

- (void)gasAvailabilityChanged:(GasAvailabilityPresentable *)status
{
    self.clockLabel.text = status.labelText;
    self.clockLabel.backgroundColor = status.backgroundColor;
}

#pragma mark - Properties

- (UILabel *)clockLabel
{
    if (!_clockLabel){
        _clockLabel = [UILabel new];
        _clockLabel.backgroundColor = [UIColor greenColor];
    }
    return _clockLabel;
}

- (InMemoryDateGateway *)dateGateway
{
    if (!_dateGateway){
        _dateGateway = [InMemoryDateGateway new];
    }
    return _dateGateway;
}


- (GasStationUseCase *)useCase
{
    if (!_useCase) {
        _useCase = [[GasStationUseCase alloc] initWithDateGateway:self.dateGateway];
    }
    return _useCase;
}

@end