#import "InMemoryDateGateway.h"

@implementation InMemoryDateGateway

#pragma mark - DateGateway

- (RACSignal *)dateForInterval:(NSTimeInterval)interval
{
    return [RACSignal interval:interval onScheduler:[RACScheduler mainThreadScheduler]];
}

@end