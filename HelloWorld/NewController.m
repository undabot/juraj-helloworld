#import "NewController.h"



@interface NewController ()

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *countTitleLabel;
@property (strong, nonatomic) UIButton *appendButton;
@end


@implementation NewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addLabels];
    [self addButton];
}

- (void)addLabels
{
    self.titleLabel.text = @"Hello World";
    
    [self updateHeightOfLabel:self.titleLabel];
    [self updateCountLabelForString:self.titleLabel.text];
    
    [self.view addSubview:self.titleLabel];
    [self.view addSubview:self.countTitleLabel];
}

- (void)addButton
{
    [self.appendButton addTarget:self action:@selector(appendButtonClick) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:self.appendButton];
}

- (void)appendButtonClick
{
    [self appendTitleLabelString];
    [self updateCountLabelForString:self.titleLabel.text];
    
    [self updateHeightOfLabel:self.titleLabel];
    [self changeOriginOnLabel:self.countTitleLabel onButton:self.appendButton whenIntersectedWithLabel:self.titleLabel];
}

- (void)updateCountLabelForString:(NSString *)title
{
    self.countTitleLabel.text = [NSString stringWithFormat:@"%lu", title.length];    
}

- (void)updateHeightOfLabel:(UILabel *)label
{
    CGSize size = [label sizeThatFits:CGSizeMake(label.frame.size.width, CGFLOAT_MAX)];
    
    CGRect titleLabelFrame = label.frame;
    titleLabelFrame.size.height = size.height;
    label.frame = titleLabelFrame;
}

- (void)appendTitleLabelString
{
    NSMutableString *newTitleLabelString = [self.titleLabel.text mutableCopy];
    [newTitleLabelString appendString:newTitleLabelString];
    self.titleLabel.text = newTitleLabelString;
}

- (void)changeOriginOnLabel:(UILabel *)label onButton:(UIButton *)button whenIntersectedWithLabel:(UILabel *)targetLabel
{
    if (CGRectIntersectsRect(label.frame, targetLabel.frame)){
        CGRect countTitleLabelFrame = label.frame;
        CGRect appendButtonFrame = button.frame;
        CGFloat y = CGRectGetMaxY(self.titleLabel.frame)+ 10;
    
        countTitleLabelFrame.origin.y = y;
        appendButtonFrame.origin.y = y;
    
        [label setFrame:countTitleLabelFrame];
        [button setFrame:appendButtonFrame];
    }

}

#pragma mark - Properties

- (UILabel*)titleLabel
{
    if (!_titleLabel){
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, CGRectGetWidth(self.view.frame), 20)];
        _titleLabel.lineBreakMode = NSLineBreakByCharWrapping;
        _titleLabel.numberOfLines = 0;
    }
    return _titleLabel;
}

- (UILabel*)countTitleLabel
{
    if (!_countTitleLabel){
        _countTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 50, 200, 40)];
    }
    return _countTitleLabel;
}

- (UIButton*)appendButton
{
    if (!_appendButton){
        _appendButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        _appendButton.frame = CGRectMake(10, 50, 200, 40);
        [_appendButton setTitle:@"Button" forState:UIControlStateNormal];
    }
    return _appendButton;
}

@end
