//libs
#import <Specta/Specta.h>
#import <Expecta/Expecta.h>
#import <OCHamcrest/OCHamcrest.h>
#import <OCMockito/OCMockito.h>

//use case
#import "GasStationUseCase.h"
#import "DateGateway.h"
#import "GasAvailabilityPresentable.h"


SpecBegin(GasStationUseCase)

describe(@"Use case", ^{
    __block id <DateGateway> gateway = MKTMockProtocol(@protocol(DateGateway));
    __block GasStationUseCase *useCase = [[GasStationUseCase alloc] initWithDateGateway:gateway];
    __block GasAvailabilityPresentable *gasAvailabilityPresentable;
    __block NSDateComponents *comps = [[NSDateComponents alloc] init];
    
    context(@"when hour is even", ^{
        beforeEach(^{
            [comps setHour:11];
            [comps setMinute:55];
            [comps setSecond:20];
            
            [given([gateway dateForInterval:1]) willReturn:[RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
                [subscriber sendNext:[[NSCalendar currentCalendar] dateFromComponents:comps]];
                return nil;
            }]];
            
            [[useCase gasAvailabilityChangedSignal] subscribeNext:^(GasAvailabilityPresentable *gasPresentable) {
                gasAvailabilityPresentable = gasPresentable;
            }];
        });
        
        it(@"will return gasAvailability with labeText '11:55:20'.", ^{
            expect(gasAvailabilityPresentable.labelText).to.equal(@"11:55:20");
        });
        
        it(@"will return gasAvailability with backgorund color red.", ^{
            expect(gasAvailabilityPresentable.backgroundColor).to.equal([UIColor redColor]);
        });
    });
    
    context(@"when hour is odd", ^{
        beforeEach(^{
            [comps setHour:12];
            [comps setMinute:55];
            [comps setSecond:20];
            
            [given([gateway dateForInterval:1]) willReturn:[RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
                [subscriber sendNext:[[NSCalendar currentCalendar] dateFromComponents:comps]];
                return nil;
            }]];
            
            [[useCase gasAvailabilityChangedSignal] subscribeNext:^(GasAvailabilityPresentable *gasPresentable) {
                gasAvailabilityPresentable = gasPresentable;
            }];
        });
        
        it(@"will return gasAvailability with labeText '12:55:20'.", ^{
            expect(gasAvailabilityPresentable.labelText).to.equal(@"12:55:20");
        });
        
        it(@"will return gasAvailability with backgorund color green.", ^{
            expect(gasAvailabilityPresentable.backgroundColor).to.equal([UIColor greenColor]);
        });
    });
});

SpecEnd